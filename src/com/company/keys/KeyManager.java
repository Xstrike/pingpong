package com.company.keys;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class KeyManager implements KeyListener {
    /* singelton pattern */

    private static KeyManager keymanager = new KeyManager();

    private static boolean[] keys;
    private static boolean up;
    private static boolean down;

    private KeyManager(){
        keys = new boolean[256];
    }

    public static KeyManager getInstance(){
        return keymanager;
    }

    public static void tick(){
        up = keys[KeyEvent.VK_W];
        down = keys[KeyEvent.VK_S];
    }

    public static boolean getUp(){
        return up;
    }

    public static boolean getDown(){
        return down;
    }

    @Override
    public void keyTyped(KeyEvent e) {}

    @Override
    public void keyPressed(KeyEvent e) {
        keys[e.getKeyCode()] = true;
    }

    @Override
    public void keyReleased(KeyEvent e) {
        keys[e.getKeyCode()] = false;
    }
}
