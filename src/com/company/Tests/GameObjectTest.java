package com.company.Tests;

import com.company.objects.GameObject;
import com.company.objects.ID;
import com.company.objects.Player;
import org.junit.Assert;
import org.junit.Test;

import java.awt.*;

import static org.junit.Assert.*;

public class GameObjectTest {
    private GameObject gameObject = new Player(new Point(1, 2), new Dimension(10, 10),5);

    @Test
    public void getPoint() {
        assertEquals(gameObject.getPoint(), new Point(1,2));
    }

    @Test
    public void getRectangle() {
        assertEquals(gameObject.getRectangle(), new Rectangle(1,2,10,10));
    }

    @Test
    public void getId() {
        Assert.assertEquals(gameObject.getId(), ID.ID_player);
    }
}