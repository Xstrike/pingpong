package com.company.Tests;

import com.company.constants.Constants;
import com.company.objects.Ball;

import java.awt.*;

import static org.junit.Assert.*;

public class BallTest {

    private Ball ball = new Ball(new Point(1,5), new Dimension(10,10));

    @org.junit.Test
    public void getVelx() {
        assertEquals(ball.getVelx(), Constants.BALL_VELX);
    }

    @org.junit.Test
    public void getGameOver() {
        assertEquals(ball.getGameOver(), false);
    }
}