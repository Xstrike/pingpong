package com.company.background;


import com.company.constants.Constants;

import com.company.keys.KeyManager;

import java.awt.*;
import java.awt.image.BufferStrategy;

public class GamePanel implements Runnable {

    private Window window;
    private Thread thread;
    private boolean running = false;
    private BufferStrategy bs;
    private Graphics g;

    private GamePlay gamePlay;

    public GamePanel(){}

    private void init(){
        window = new Window();
        window.getWindow().addKeyListener(KeyManager.getInstance());
        gamePlay = new GamePlay();
    }

    private void update(){
        gamePlay.update();
        KeyManager.tick();
    }

    private void draw(){
        bs = window.getCanvas().getBufferStrategy();
        if(bs == null){
            window.getCanvas().createBufferStrategy(3);
            return;
        }
        g = bs.getDrawGraphics();

        gamePlay.draw(g);

        bs.show();
        g.dispose();
    }

    public void run() {
        init();

        double tickTime = 1000000000 / Constants.AMOUNT_OF_TICKS;
        double delta = 0;
        long now;
        long lastTime = System.nanoTime();

        int fps = 0;
        long fpsTime = 0;


        while(running){
            now = System.nanoTime();
            delta += (now - lastTime) / tickTime;
            fpsTime += now - lastTime;
            lastTime = now;
            if(delta >= 1)
            {
                update();
                draw();
                fps++;
                delta--;
            }

            if(fpsTime >= 1000000000)
            {
                System.out.println(fps);
                fps = 0;
                fpsTime = 0;
            }


        }

        stop();
    }

    public synchronized void start(){
        if(!running)
        {
            running = true;
            thread = new Thread(this);
            thread.start();
        }
    }

    public synchronized void stop(){
        if(!running) return;

        running = false;
        try{
            thread.join();
        }catch (Exception e){}
    }
}
