package com.company.background;

import com.company.constants.Constants;
import com.company.objects.Ball;
import com.company.objects.Enemy;
import com.company.objects.Player;
import com.company.objects.Scene;

import java.awt.*;


public class GamePlay extends Scene {

    private Image background;
    private Player player;
    private Enemy enemy;
    private Ball ball;

    public GamePlay()
    {
        ball = new Ball(Constants.BALL_POINT, Constants.BALL_DIMENSIONS);
        player = new Player(Constants.PLAYER_POINT,Constants.PLAYER_DIMENSIONS, Constants.PLAYER_SPEED);
        enemy = new Enemy(Constants.ENEMY_POINT, Constants.PLAYER_DIMENSIONS, Constants.ENEMY_SPEED);
        background = Toolkit.getDefaultToolkit().getImage(Constants.BACKGROUND_PATH);



    }

    public void draw(Graphics g)
    {
        g.drawImage(background,0,0,Constants.WINDOW_DIMENSIONS.width,Constants.WINDOW_DIMENSIONS.height,null);
        player.draw(g);
        enemy.draw(g);
        ball.draw(g);
    }

    public void update()
    {
        player.update();
        enemy.update(ball.getRectangle(), ball.getVelx());
        ball.update(player.getRectangle(),enemy.getRectangle());
        if(ball.getGameOver()) ball = new Ball(Constants.BALL_POINT, Constants.BALL_DIMENSIONS);
    }
}
