package com.company.background;

import com.company.constants.Constants;

import javax.swing.*;
import java.awt.*;


public class Window{

    private JFrame window;
    private Canvas canvas;


    public Window()
    {
        window = new JFrame(Constants.WINDOW_TITLE);
        InitWindow();

        InitCanvas();
    }

    private void InitWindow()
    {
        window.setSize(Constants.WINDOW_DIMENSIONS);
        window. setDefaultCloseOperation(window.EXIT_ON_CLOSE);
        window.setResizable(false);
        window.setLocationRelativeTo(null);
        window.setVisible(true);

    }

    public Canvas getCanvas() {
        return canvas;
    }

    private void InitCanvas()
    {
        canvas = new Canvas();
        canvas.setPreferredSize(Constants.WINDOW_DIMENSIONS);
        canvas.setMaximumSize(Constants.WINDOW_DIMENSIONS);
        canvas.setMinimumSize(Constants.WINDOW_DIMENSIONS);
        canvas.setFocusable(false);

        window.add(canvas);
        window.pack();
    }

    public JFrame getWindow() {
        return window;
    }


}
