package com.company.objects;

import com.company.constants.Constants;
import com.company.keys.KeyManager;

import java.awt.*;
import java.util.Random;

public class Ball extends GameObject {

    private boolean start;
    private boolean gameOver;
    private int velx;
    private int vely;

    public Ball(Point point, Dimension dimension)
    {
        super(point, dimension);
        id = ID.ID_ball;
        start = false;
        gameOver = false;
        Random random = new Random();
        if(random.nextInt() * 10 < 5) velx = Constants.BALL_VELX;
            else velx = Constants.BALL_VELX * -1;
        vely = Constants.BALL_VELY;
    }

    public void draw(Graphics g)
    {
        g.setColor(Color.RED);
        g.fillOval(rectangle.x,rectangle.y,rectangle.width,rectangle.height);
    }

    public void update(Rectangle player, Rectangle enemy){
        if((KeyManager.getUp() || KeyManager.getDown()) && !start){
            start = true;
        }
        if(start) {
            if (velx > 0) colide(enemy);
            if (velx < 0) colide(player);
            colideWall();

            rectangle.x += velx;
            rectangle.y += vely;
        }

    }

    private void colideWall()
    {
        if(rectangle.x <= 0) gameOver = true;
        if(rectangle.y <= 0) vely *= -1;
        if(rectangle.x + rectangle.width >= Constants.WINDOW_DIMENSIONS.width) gameOver = true;
        if(rectangle.y + rectangle.height >= Constants.WINDOW_DIMENSIONS.height) vely *= -1;
    }

    private void colide(Rectangle objectRect){
        if(objectRect.intersects(rectangle)) velx *= -1;
    }

    public int getVelx(){
        return velx;
    }

    public Boolean getGameOver(){
        return gameOver;
    }
}
