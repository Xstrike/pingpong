package com.company.objects;


import com.company.constants.Constants;
import com.company.keys.KeyManager;


import java.awt.*;

public class Player extends GameObject {

    private int speed;

    public Player(Point point, Dimension dimension, int speed)
    {
        super(point, dimension);
        this.speed = speed;
        id = ID.ID_player;
    }

    @Override
    public void draw(Graphics g)
    {
        g.setColor(Color.BLUE);
        g.drawRect(rectangle.x,rectangle.y,rectangle.width,rectangle.height);
    }

    @Override
    public void update(){
        if(KeyManager.getUp())
        {
            if(rectangle.y > 0) rectangle.y -= speed;
        }
        if(KeyManager.getDown())
        {
            if(rectangle.y + rectangle.height < Constants.WINDOW_DIMENSIONS.height) rectangle.y += speed;
        }
    }


}
