package com.company.objects;

import java.awt.*;

public abstract class GameObject {
    protected Point point;
    protected Rectangle rectangle;
    protected ID id;//

    public GameObject(Point point, Dimension dimension){
        this.point = point;
        this.rectangle = new Rectangle(point.x,point.y,dimension.width,dimension.height);
    }

    public void draw(Graphics g){}

    public void update(){}

    public Point getPoint() { return point; }

    public Rectangle getRectangle() {
        return rectangle;
    }

    public ID getId(){
        return id;
    }
}
