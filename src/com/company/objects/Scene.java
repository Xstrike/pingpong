package com.company.objects;

import java.awt.*;

public abstract class Scene {

    public void draw(Graphics g){}

    public void update(){}
}
