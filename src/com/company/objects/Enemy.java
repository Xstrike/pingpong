package com.company.objects;

import java.awt.*;

public class Enemy extends GameObject{

    private int speed;

    public Enemy(Point point, Dimension dimension, int speed)
    {
        super(point,dimension);
        id = ID.ID_enemy;
        this.speed = speed;
    }

    public void draw(Graphics g)
    {
        g.setColor(Color.BLUE);
        g.drawRect(rectangle.x,rectangle.y,rectangle.width,rectangle.height);
    }

    public void update(Rectangle ball, int velx){
        if(velx > 0)
        {
            if(ball.y + ball.height > rectangle.y + rectangle.height) rectangle.y += speed;
            if(ball.y < rectangle.y) rectangle.y -= speed;
        }
    }
}
