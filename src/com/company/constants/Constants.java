package com.company.constants;

import java.awt.*;

public class Constants {
    //Window
    public static final Dimension WINDOW_DIMENSIONS = new Dimension(640,320);
    public static final String WINDOW_TITLE = "Ping-Pong";
    public static final String BACKGROUND_PATH = "res/imgs/arena2.png";
    /**/

    //Frames
    public static final double AMOUNT_OF_TICKS = 30;
    /**/

    //Player & Enemy
    public static Point PLAYER_POINT = new Point(30, 160 - 25);
    public static Point ENEMY_POINT = new Point(640-30-5, 160 - 25);
    public static Dimension PLAYER_DIMENSIONS = new Dimension(5, 50);
    public static final int PLAYER_SPEED = 5;
    public static final int ENEMY_SPEED = 5;
    /**/

    //Ball
    public static Point BALL_POINT = new Point(WINDOW_DIMENSIONS.width / 2 - 5, WINDOW_DIMENSIONS.height / 2 - 5);
    public static Dimension BALL_DIMENSIONS = new Dimension(10, 10);
    public static int BALL_VELX = 3;
    public static int BALL_VELY = 4;
    /**/
}
